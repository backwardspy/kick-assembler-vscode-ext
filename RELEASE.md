# Release Notes
## v0.5.5
### The Mega65 Release

Big thanks to [backwardspy](https://gitlab.com/backwardspy) for adding support for Byte Dumps in Kick Assembler. For more information on what the Byte Dump can do, read more information [here.](http://theweb.dk/KickAssembler/webhelp/content/cpt_QuickReference.html)

Thanks to [Lubber](https://gitlab.com/lubber) added some initial support for the Mega65. This will allow you to use the `.cpu` directive, and adds things like syntax highlighting and autocompletion/snippet support. To use this feature, you will need to use an [alternative Kick Assembler Jar.](https://gitlab.com/jespergravgaard/kickassembler65ce02) He also added a new Reference Provider that will show you all the places where a variable or macro might be used in your code, a very helpful feature indeed.

![](/images/referencesprovider.gif)

Press SHIFT-F12 to show the list of Reference in the Editor, or alternately press SHIFT-ALT-F12 to show them in a separate View pane. This is an extremely helpful way to find out where all those Zero Page variables you created are.

There were also a few fixes to the Hover Provider to include Scoped labels, and macro/function parameters.


For a full list of changes, please see the projects [History](HISTORY.md) file.

We hope you are enjoying the Extension, and if you find any bugs, or would like to see a certain feature added, please feel free to use our [Trello Board](https://trello.com/b/vIsioueo/kick-assembler-vscode-ext#) or the [Gitlab](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues) issues page.

Enjoy the new release!
